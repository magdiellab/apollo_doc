# Documentação APOLLOX API - GATEWAY DE PAGAMENTO

Documentação da API - ApolloX, com ela será possível;
  - Aceitar centenas de moedas, não somente BTC.
  - Criar faturas 'pendentes'
  - Consultar status de faturas
  - Ao pagar a fatura, receber um POST de notificação
  - Gerar QRCODE

### EndPoints

Segue a lista de endpoints, a URL base será sempre
```` http://kublaicoin.io/btc/v1 ````

##### Criando Requisição de Pagamento (Tipo GET)

```` /create_transaction ````

```sh
#http://kublaicoin.io/btc/v1/create_transaction?currency1=btc&currency2=btc&amount=1&item_number=983&invoice=12&custom=Fatura%20de%20Pagamento

currency1 - Moeda que está sendo enviada
currency2 - Moeda que será recebida (utilize sempre BTC, exceto se quiser converter o BTC recebido em ETH, por exemplo, então ai você colocar currency1=btc e currency2=eth para ocorrer a conversão.)
amount - Valor a ser pago
item_number - Identificador para o número do pagamento (utilize um padrão qualquer)
invoice - Código da Fatura (utilize um padrão que desejar)
custom - Qualquer descrição que achar necessário. 

Os campos não podem ficar em branco.

```

### Gerar QRCODE

````/qrcode````

```sh
#http://kublaicoin.io/btc/v1/qrcode?address=address&value=1

address - Endereço para pagar
value - Quantia a pagar

Os campos não podem ficar em branco.

```

### Consultar Faturas

````/invoice_info````

```sh
#http://kublaicoin.io/btc/v1/invoice_info?invoice_id=CPEI5HX0NCVAKZX41ANRM9HZKE

invoice_id - ID da fatura (esse ID é fornecido na geração da fatura na request anterior)

Os campos não podem ficar em branco.

```



❤️
